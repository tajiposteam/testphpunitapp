<?php
require 'Calculate.php';
 
class CalculatorTests extends PHPUnit_Framework_TestCase
{
    private $calculator;
 
    protected function setUp()
    {
        $this->calculator = new Calculator();
    }
 
    protected function tearDown()
    {
        $this->calculator = NULL;
    }
 
    public function testAdd()
    {
        $result = $this->calculator->add(1, 2);
        $this->assertEquals(3, $result);
    }
	
	
    public function testSub()
    {
        $result = $this->calculator->sub(1, 2);
        $this->assertEquals(-1, $result);
    }
	
	public function testDiv()
    {
        $result = $this->calculator->div(1, 2);
        $this->assertEquals(0.8, $result);
    }
	
	public function testMul()
    {
        $result = $this->calculator->mul(1, 2);
        $this->assertEquals(2, $result);
    }
 
}
 

?>